let app = new PIXI.Application({
    width: 1000,
    height: 700
});

const SPEED = 2;

let roadContainer = [], userPos = {x: 9999, y: 9999}, xMove = 0, yMove = 0, roundRoadContainer = [];

document.body.appendChild(app.view);

let options = {
    additional: [
        {
            url: "assets/img/tree.png",
            x: 100,
            y: 500
        },
        {
            url: "assets/img/tree.png",
            x: 550,
            y: 300
        },
    ],
    cellSize: 10,
    roadSize: 50,
    width: 1000,
    height: 700,
    playerSize: 20,
    startCoord: {
        x: 50,
        y: 330
    },
    winCoord: {
        x: 967,
        y: 518
    },
    bgUrl: "assets/img/grass2.jpg",
    roadUrl: "assets/img/field.png",
    roundRoadUrl: "assets/img/roundRoad.png",
    playerUrl: "assets/img/user.png",
    treeUrl: "assets/img/tree.png",
    enemyUrl: "assets/img/enemy.png",
    coords: [
        {
            x: 0,
            y: 300
        },
        {
            x: 50,
            y: 300
        },
        {
            x: 100,
            y: 300
        },
        {
            x: 150,
            y: 300
        },
        {
            x: 200,
            y: 300
        },
        {
            x: 250,
            y: 300
        },
        {
            x: 300,
            y: 300
        },
        {
            x: 350,
            y: 300
        },
        {
            x: 400,
            y: 150
        },
        {
            x: 400,
            y: 200
        },
        {
            x: 400,
            y: 250
        },
        {
            x: 400,
            y: 300
        },
        {
            x: 400,
            y: 350
        },
        {
            x: 400,
            y: 400
        },
        {
            x: 400,
            y: 450
        },
        {
            x: 400,
            y: 500
        },
        {
            x: 450,
            y: 500
        },
        {
            x: 500,
            y: 500
        },
        {
            x: 550,
            y: 500
        },
        {
            x: 600,
            y: 500
        },
        {
            x: 650,
            y: 500
        },
        {
            x: 700,
            y: 500
        },
        {
            x: 750,
            y: 500
        },
        {
            x: 800,
            y: 500
        },
        {
            x: 850,
            y: 500
        },
        {
            x: 900,
            y: 500
        },
        {
            x: 950,
            y: 500
        },
        {
            x: 200,
            y: 250
        },
        {
            x: 200,
            y: 200
        },
        {
            x: 200,
            y: 150
        },
        {
            x: 200,
            y: 100
        },
        {
            x: 250,
            y: 100
        },
        {
            x: 300,
            y: 100
        },
        {
            x: 350,
            y: 100
        },
        {
            x: 400,
            y: 100
        },
        {
            x: 450,
            y: 100
        },
        {
            x: 500,
            y: 100
        },
        {
            x: 550,
            y: 100
        },
        {
            x: 600,
            y: 100
        },
        {
            x: 650,
            y: 100
        },
        {
            x: 700,
            y: 100
        },
        {
            x: 700,
            y: 150
        },
        {
            x: 700,
            y: 200
        },
        {
            x: 700,
            y: 250
        },
        {
            x: 750,
            y: 300
        },
        {
            x: 800,
            y: 300
        },
        {
            x: 850,
            y: 300
        },
        {
            x: 900,
            y: 300
        },
        {
            x: 900,
            y: 350
        },
        {
            x: 900,
            y: 400
        },
        {
            x: 900,
            y: 450
        },
        {
            x: 700,
            y: 300
        },
        {
            x: 700,
            y: 350
        },
        {
            x: 700,
            y: 400
        },
        {
            x: 700,
            y: 450
        }
    ]
}

let map = [];

function Game(options) {
    // options.coords.forEach(elem => {
    //     map.push(elem.x.toString() + " " + elem.y.toString());
    // });

    for (let i = 0; i < options.height / options.roadSize; i++) {
        let mas = [];
        for (let j = 0; j < options.width / options.roadSize; j++) {
            mas.push(0);
        }
        map.push(mas);
    }

    // options.coords.forEach((element) => {
        // console.log(element.y / options.roadSize, element.x / options.roadSize)
        // map[element.y / options.roadSize][element.x / options.roadSize] = 1;
    // });

    // console.log(map)

    PIXI.loader
        .add([
            options.bgUrl,
            options.roadUrl,
            options.playerUrl,
            options.treeUrl,
            options.roundRoadUrl,
            options.enemyUrl,
        ])
        .load(setup);

    function setup() {   
        for (let i = 0; i < options.width / options.cellSize; i++) {
            for (let j = 0; j < options.height / options.cellSize; j++) {
                let bg = new PIXI.Sprite(PIXI.loader.resources[options.bgUrl].texture);
                bg.width = options.cellSize;
                bg.height = options.cellSize;
                bg.x = i * options.cellSize;
                bg.y = j * options.cellSize;
                app.stage.addChild(bg);
            }
        }

        options.additional.forEach(element => {
            let tree = new PIXI.Sprite(PIXI.loader.resources[options.treeUrl].texture);
            tree.x = element.x;
            tree.y = element.y;
            app.stage.addChild(tree);
        });

        let player = new PIXI.Sprite(PIXI.loader.resources[options.playerUrl].texture);
        player.height = options.playerSize;
        player.width = options.playerSize;
        player.x = options.startCoord.x;
        player.y = options.startCoord.y;
        player.interactive = true;
        player.buttonMode = true;
        player.anchor.set(0.5);
        player
            .on("pointerdown", onDragStart)
            .on("pointerup", onDragEnd)
            .on("pointerupoutside", onDragEnd)
            .on("pointermove", onDragMove);

        let enemy = new PIXI.Sprite(PIXI.loader.resources[options.enemyUrl].texture);
        enemy.interactive = true;
        enemy.buttonMode = true;
        enemy.x = options.winCoord.x;
        enemy.y = options.winCoord.y + 6;
        enemy.width = 20;
        enemy.height = 20;

        var flag = true;
        let vision = 100;
        let direct = "";

        let enemyIndex = {
            i: 10,
            j: 19
        }

        // setInterval(() => {
        //     console.log(enemyIndex.i,enemyIndex.j)
        //     if (map[enemyIndex.i][enemyIndex.j - 1] == 1 && direct != "x+") {
        //         enemyIndex.j -= 1;
        //         enemy.x -= options.roadSize;
        //         // for (let i = 0; i < 10; i++) {
        //         // }
        //         direct = "x-";
        //     } else if (map[enemyIndex.i][enemyIndex.j + 1] == 1 && direct != "x-") {
        //         enemyIndex.j += 1
        //         enemy.x += options.roadSize;
        //         enemy.x -= options.roadSize;
        //         // for (let i = 0; i < 10; i++) {
        //         // }
        //         direct = "x+";
        //     } else if (map[enemyIndex.i - 1][enemyIndex.j] == 1 && direct != "y+") {
        //         enemyIndex.i -= 1;
        //         enemy.y -= options.roadSize;
        //         enemy.x -= options.roadSize;
        //         // for (let i = 0; i < 10; i++) {
        //         // }
        //         direct = "y-";
        //     } else if (map[enemyIndex.i + 1][enemyIndex.j] == 1 && direct != "y-") {
        //         enemyIndex.i += 1;
        //         enemy.y += options.roadSize;
        //         enemy.x -= options.roadSize;
        //         // for (let i = 0; i < 10; i++) {
        //         // }
        //         direct = "y+";
        //     }
        // }, 500);

        options.coords.forEach(coord => {
            let road, neighbors = [];

            options.coords.forEach((elem) => {
                if (elem.x == coord.x + options.roadSize && elem.y == coord.y) {
                    neighbors.push("right");
                }
                if (elem.x == coord.x && elem.y == coord.y + options.roadSize) {
                    neighbors.push("bottom");
                }
                if (elem.x == coord.x - options.roadSize && elem.y == coord.y) {
                    neighbors.push("left");
                }
                if (elem.x == coord.x && elem.y == coord.y - options.roadSize) {
                    neighbors.push("top");
                }
            });

            // console.log(neighbors, coord)

            let xOffset = 0, yOffset = 0;

            if (neighbors.length != 2) {
                road = new PIXI.Sprite(PIXI.loader.resources[options.roadUrl].texture);
            } else {
                if (neighbors.includes("left") && neighbors.includes("top")) {
                    road = new PIXI.Sprite(PIXI.loader.resources[options.roundRoadUrl].texture);
                    road.angle = 180;
                    xOffset = options.roadSize;
                    yOffset = options.roadSize;
                    // road.name = neighbors.join("-");
                    // roundRoadContainer.push(road);
                } else
                if (neighbors.includes("top") && neighbors.includes("right")) {
                    road = new PIXI.Sprite(PIXI.loader.resources[options.roundRoadUrl].texture);
                    road.angle = 0;
                    // yOffset = options.roadSize;
                    // road.name = neighbors.join("-");
                    // roundRoadContainer.push(road);
                } else
                if (neighbors.includes("right") && neighbors.includes("bottom")) {
                    road = new PIXI.Sprite(PIXI.loader.resources[options.roundRoadUrl].texture);
                    road.angle = 90;
                    xOffset = options.roadSize;
                    // road.name = neighbors.join("-");
                    // roundRoadContainer.push(road);
                } else
                if (neighbors.includes("bottom") && neighbors.includes("left")) {
                    road = new PIXI.Sprite(PIXI.loader.resources[options.roundRoadUrl].texture);
                    road.angle = 180;
                    yOffset = options.roadSize;
                    xOffset = options.roadSize;
                    
                } else {
                    road = new PIXI.Sprite(PIXI.loader.resources[options.roadUrl].texture);
                    // roundRoadContainer.push(road);
                    // road.name = neighbors.join("-");
                }
            }

            let point = new PIXI.Graphics();
            point.beginFill();
            point.drawCircle(0, 0, 1);
            point.endFill();

            road.name = neighbors.join("-");
            point.name = road.name;
            // roundRoadContainer.push(road);

            road.width = options.roadSize;
            road.height = options.roadSize;
            road.x = coord.x + xOffset;
            road.y = coord.y + yOffset;

            point.x = road.x + options.roadSize / 2 - xOffset;
            point.y = road.y + options.roadSize / 2 - yOffset;

            // console.log(neighbors)

            


            // let roundRoad = new PIXI.Sprite(PIXI.loader.resources[options.roundRoadUrl].texture);

            // road.angle = 90;

            let container = new PIXI.Container();

            container.addChild(road);
            roundRoadContainer.push(road);
            
            if (((neighbors.includes("right") && neighbors.includes("left")) || 
            (neighbors.includes("bottom") && neighbors.includes("top"))) && neighbors.length == 2) {
                
            } else {
                roadContainer.push(point);
                app.stage.addChild(point);
            }
            app.stage.addChild(container);
            app.stage.addChild(player);
            enemy.anchor.set(0.5);
            app.stage.addChild(enemy);
        });
        app.ticker.add(delta => gameLoop(delta, enemy, options));
    }
    
}

function randomInteger(min, max) {
    // случайное число от min до (max+1)
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

let oldDistance = 0, cont = true;

xMove = -2;
let prevRoad = {
    name: "disable"
};

let direct;

function gameLoop(delta, enemy, options) {
    enemy.x += xMove;
    enemy.y += yMove;
    roadContainer.forEach((element) => {
        // console.log(prevRoad == element)
        if (element != prevRoad) {
            if (element.containsPoint({
                x: enemy.x,
                y: enemy.y
            })) {
                let sosedi = element.name.split("-");
                if (xMove == SPEED && sosedi.includes("left")) {
                    sosedi.splice(sosedi.indexOf("left"), 1);
                }
                if (xMove == -SPEED && sosedi.includes("right")) {
                    sosedi.splice(sosedi.indexOf("right"), 1);
                }
                if (yMove == SPEED && sosedi.includes("top")) {
                    sosedi.splice(sosedi.indexOf("top"), 1);
                }
                if (yMove == -SPEED && sosedi.includes("bottom")) {
                    sosedi.splice(sosedi.indexOf("bottom"), 1);
                }
                if (sosedi.length == 0) {
                    switch (direct) {
                        case "left": xMove = SPEED; yMove = 0; direct = "right"; break;
                        case "right": xMove = -SPEED; yMove = 0; direct = "left"; break;
                        case "top": yMove = SPEED; xMove = 0; direct = "bottom"; break;
                        case "bottom": yMove = -SPEED; xMove = 0; direct = "top"; break;
                    }
                } else {
                    direct = sosedi[randomInteger(0, sosedi.length - 1)];
                    prevRoad = element;
                    switch (direct) {
                        case "left": xMove = -SPEED; yMove = 0; break;
                        case "right": xMove = SPEED; yMove = 0; break;
                        case "top": yMove = -SPEED; xMove = 0; break;
                        case "bottom": yMove = SPEED; xMove = 0; break;
                    }                
                }
                console.log(sosedi);
            }
        }
    });
} 

Game(options);

function onDragStart(event) {
    this.data = event.data;
    this.event = event;
    this.dragging = true;
}

function onDragEnd() {
    this.alpha = 1;
    this.dragging = false;
    this.data = null;
}

function onDragMove() {
    if (this.dragging) {
        const newPos = this.data.getLocalPosition(this.parent);
        roundRoadContainer.forEach(element => {
            if (element.containsPoint({
                x: newPos.x,
                y: newPos.y
            })  && this.x <= newPos.x + 100 && this.x >= newPos.x - 100
                && this.y >= newPos.y - 100 && this.y <= newPos.y + 100) {
                    setTimeout(() => {
                        this.x = newPos.x;
                        this.y = newPos.y;  
                        userPos = {
                            x: newPos.x,
                            y: newPos.y
                        } 
                    }, 100);
            }
        });
        if ((this.x > options.winCoord.x - 50 && this.x < options.winCoord.x + 50) 
            && (this.y > options.winCoord.y - 50 && this.y < options.winCoord.y + 50)) {
            console.log("Finish")
        }
    }
}